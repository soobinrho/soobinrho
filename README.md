<br>
<br>

> A college student from South Korea
> studying at Augustana University, South Dakota, majoring in
> [***P***]hilosophy, mat[***h***], and
> [***D***]ata science -- or PhD as
> I like to call it 😎

<br>

# Overview

I made too many mistakes of making
unrealistic, unspecific goals.
In my college sophomore year, a shower
thought struck my mind:
I gotta make realistic, specific goals
and put these on my GitHub.


This page keeps track of all my life
goals. Plus, I use this page
also to show what kind of projects
I'm working on with as many pictures and
and as few words as possible.

<br>

# Projects

#### Nsustain [[GitHub](https://github.com/Nsustain)]

🌳 Nsustain is a website where volunteer open-source developers can come help farmers and scientists around the world.

<p align="center">
  <b>
    YouTube<br>
    [<a href="https://youtu.be/E8JAcw1SJLA">What is Nsustain?</a>]
  </b>
  <a href="https://youtu.be/E8JAcw1SJLA" target="_blank">
    <img src="https://img.youtube.com/vi/E8JAcw1SJLA/maxresdefault.jpg" alt="Nsustain YouTube" />
  </a>
</p>

<br>

#### LaTeX Template for College Assignments [[GitHub](https://github.com/soobinrho/latex-college-assignments-template)]

This is a $\LaTeX$ template based on the
`North American GeoGebra Journal Template` and
JupyterLab's code-cells preambles.
You can insert JupyterLab style code blocks,
as well as GitHub style inline codes like `this inline code block`.

Personally, I use this for my math assignments, but since its
code blocks look exactly the same as JupyterLab's code cells,
it can be used for computer science assignments as well,
if you happen to be the kind of person who likes
to write everyting in $\LaTeX$.

<img src="https://user-images.githubusercontent.com/19341857/174758273-d4410ce1-afe4-4d4e-8229-1f069ee8a165.png" width="500px">

<br>

#### College Coursework Programming [[GitHub](https://github.com/soobinrho/college-programming)]

This repository is a collection of all codes I wrote for my college courses,
such as *Computer Science I & II*, *C++ Programming*, and *Calculus II*.
Most of the graph generation was done using Python. Most of the pdf documents were
written with $\LaTeX$.

<img src="https://user-images.githubusercontent.com/19341857/176699968-c9cd420b-b0f9-47d5-98cb-320e533e9907.png" width="500px">

<img src="https://user-images.githubusercontent.com/19341857/184002427-9c12da89-9818-4590-b4c8-11bf8eac9032.svg" width="500px">

<br>

#### Personal Dotfiles [[GitHub](https://github.com/soobinrho/dotfiles-personal)]

Whenever I reset my computers, which
are running on KDE Fedora, I use
this repository to re-install everything.
Installing these will give you
the same look as shown below, which
consists of my chosen styles and settings for
Bash, Zsh, Konsole, Vim, and Neovim.

<img src="https://user-images.githubusercontent.com/19341857/184075267-9818b003-480e-4ceb-a172-f7e6a1d686c7.gif">

<br>

#### [Work in Progress] PowerShell Graphs Without Internet Access [[GitHub](https://github.com/soobinrho/powershell-graphs-without-internet-access)]

If you happen to be in an environment where
internet access is prohibited for security
protocol or for any other reason - e.g. military
in some countries - then this repository is for you.

Every Windows computer is likely to have
PowerShell preinstalled. This repository was
made for those who need to create graphs but
their workstations have restricted internet
access. View the codes on this repository
on your phone - or you could just print the codes -
and then type them into PowerShell ISE, which is a
program that preships with most Windows computers.
These codes will generate graphs that can
be exported to `.png` or `.jpg` files.

<br>

# Goals I'm working on

 - Master the method of loci with
the help of Joshua Foer's book
*Moonwalking with Einstein* so that
I can remember all things I want to remember.

 - Work on my mind palace twenty minutes every day.
Alternatively, use these twenty minutes either to work on my mind palace
or to plan how I am going to spend my day that day
-- e.g. no procrastination / no phone (unless necessary) until 5pm,
catch up on readings 5pm - 7pm.

<!---
**August, 2022** Again, same as the
last month. I gotta stop procrastinating.
Spent too much time on YouTube and Reddit.

**September, 2022** I reset the
two-months period for settling in a
new habit. Last month, I
utterly failed to do what I want to do.
This month, I'll try to stop my bad habit
of falling into doing what I don't wanna do.
The thing is, even if there was no Reddit
or YouTube, I would've found something else
to procrastinate with. So, it's not
their problem; it's an internal problem I have.

It'd be nice if I can just close my eyes
or even just fall a sleep, whenever I feel
an urge to open Reddit, watch YouTube, or
whatever. Best case scenario, I'll try to
work on my mind palace whenever I feel the urge.

September 10, 2022
Whenever swimming, I used to have a problem
of not being able to remember how many laps
I did. Today, I realized I can remember more
easily by using my mind palace. It turns out
I have fifteen spots for my school mind palace,
and the number of laps I do also happens to be
thirty. So, I just have to go through my
school mind palace twice to remember my laps.
-->

<br>

# Goals I had in the past

| ***When*** | ***What*** |
| ---- | ---- |
| ***2017*** | • Get full marks on my high-school IB Diploma → Failed miserably. |
| ***2018*** | • Do well in my freshman year at Augustana University → I didn't do well academically, but I met my best friends here. <br><br> • Be able to do 72 push-ups so that I can join special forces for my mandatory military service → Failed. I could do only 20. Enlisted into a regular unit in South Korean Army.
| ***2019*** | • Complete my 1<sup>st</sup> year at the army → ✓
| ***2020*** | • Complete my 2<sup>nd</sup> year at the army → ✓
| ***2021*** | • Complete my 3<sup>rd</sup> year at the army → ✓
| ***2022*** | • Complete my 4<sup>th</sup> year at the army → ✓ <br><br> • Do well in my sophomore year at Augustana University → ✓ <br><br> • Co-found Nsustain, a forum for the environment and sustainability → ✓ https://nsustain.com <br><br> • Apply to Docker-Sponsored Open Source Program → ✓ https://hub.docker.com/u/nsustain |

<!--
| ***2023*** | • Do well in my junior year → ✓ <br><br> • Get into CGI U 2023 program; learn how to grow Nsustain; make a genuine social impact → ✓ <br><br> • Reach out to farmers who need technological help; ask then if we can help them through https://nsustain.com → ✓ |

December 4, 2022, 00:35, Bergsaker Room
I noticed I tend to sound more clear when I push my
lower jaw a little bit forward. Yes! I realized
what my dentist told me when I first visited her
-- my teeth are misaligned such that my lower jaw
is a little bit behind, and this may have been
impacting my speech!

HOW DID I ADD BOLD TEXT TO LINKEDIN SUMMARY?
I used unicode bold alphabets.
Example:
  https://yaytext.com/bold-italic/

```POST
Nsustain ...  🎉 Thank you ... for ...; ... for ...; and ... for ...

𝙄𝙣 𝙘𝙖𝙨𝙚 𝙮𝙤𝙪'𝙧𝙚 𝙬𝙤𝙣𝙙𝙚𝙧𝙞𝙣𝙜 𝙬𝙝𝙖𝙩 𝙞𝙨 𝙉𝙨𝙪𝙨𝙩𝙖𝙞𝙣...
🌳 Nsustain is a forum website created for
the environment and sustainability.
https://github.com/Nsustain

𝙒𝙝𝙖𝙩 𝙙𝙤𝙚𝙨 𝙉𝙨𝙪𝙨𝙩𝙖𝙞𝙣 𝙙𝙤?
...

𝙉𝙚𝙭𝙩 𝙛𝙞𝙫𝙚 𝙮𝙚𝙖𝙧𝙨 𝙛𝙤𝙧 𝙉𝙨𝙪𝙨𝙩𝙖𝙞𝙣
• ...
• ...
• ...

What Nsustain does may not be a big help.
It's just a small help, but we're glad
we can contribute to the betterment of
environmental sustainability
in any way we can.

// Repost to original
```
-->
